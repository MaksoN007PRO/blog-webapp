## Web application (Python-Flask, Postgres) with monitoring (Prometheus, Grafana) and deployment on docker compose.

### How to deploy an application:

1. Install [docker](https://docs.docker.com/engine/install/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/linux/#install-the-plugin-manually)
2. Clone the repository to your server: ```git clone https://gitlab.com/MaksoN007PRO/blog-webapp.git```
2. Create ```./secrets/postgres_user.txt``` and ```./secrets/postgres_password.txt``` with your specific auth data for Postgres.
(Examples of such files you can find in ```./secrets/``` folder in the current repository)
3. Build images from docker-compose.yml file: ```sudo docker compose build```
4. Run the built images in the background mode: ```sudo docker compose up -d```
5. Go to http://localhost in your browser and you will see the application running.
6. To disable application type: ```sudo docker compose down```

### Demonstration of the application:
By opening the link http://localhost we get to the web application page, where we can add new posts to our blog, which will subsequently be saved in the Postgres database.

![image](/uploads/a20af4f14a92bfaa9f90aa692de1f706/image.png)


### Demonstration of the monitoring service working:
To get to the Grafana monitoring panel, go to http://localhost:3000. The default login/password pair is `admin:admin`. You must change your password the first time you successfully log in or skip this step.

![image](/uploads/dea2a2c2c5af8198b25ba1a3b72f134a/image.png)

Next, we get to the Grafana control panel and go to the Dashboards tab. Here we have already configured dashboards for monitoring various services (Postgres, Cadvisor, Node Exporter), which are imported along with the launch of docker containers. The imported dashboard `.json` files are in the `./grafana/dashboards` folder.

![image](/uploads/74d15bbe73ef02304ff47a8361922a22/image.png)

#### Description of monitoring dashboards
The `PostgreSQL Database` dashboard displays the status of PostgreSQL (version, system load, memory consumption, PostgreSQL configuration variable values, etc.)

![image](/uploads/fcc704d51da877dc0c31767a56cdc6de/image.png)

The `Node Exporter Full` dashboard displays the status of the server on which our application is running (CPU load, memory consumption, network performance, etc.)

![image](/uploads/6dcf89e9cfd39e7ff99168519a34598b/image.png)

The `Cadvisor exporter` dashboard displays the status of running containers on our server (memory usage, network traffic usage, etc.)

![image](/uploads/0490d828a81daad68af3d0762683cd16/image.png)

### Demonstration of launched targets in Prometheus:
The list of launched targets is located at http://localhost/targets?search=

![image](/uploads/71292896550b33c00795ebc5e190f406/image.png)
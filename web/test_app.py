# test_app.py

import unittest
from app import app, db
from models import Post

class FlaskAppTests(unittest.TestCase):

    def setUp(self):
        # Настройка тестового приложения и базы данных
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'  # Используем временную базу данных
        self.app = app.test_client()
        with app.app_context():
            db.create_all()  # Создаем таблицы

    def tearDown(self):
        # Удаляем все данные после каждого теста
        with app.app_context():
            db.session.remove()
            db.drop_all()

    def test_index_route(self):
        # Проверка главной страницы
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'No posts available', response.data)  # Проверяем, что нет постов

    def test_submit_post(self):
        # Проверка отправки поста
        response = self.app.post('/submit', data={'text': 'Hello, World!'})
        self.assertEqual(response.status_code, 302)  # Проверяем, что происходит редирект
        with app.app_context():
            post = Post.query.first()
            self.assertIsNotNone(post)
            self.assertEqual(post.text, 'Hello, World!')

    def test_index_route_with_posts(self):
        # Проверка главной страницы с постами
        post = Post(text='First Post')
        db.session.add(post)
        db.session.commit()

        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'First Post', response.data)  # Проверяем, что пост отображается на странице

if __name__ == '__main__':
    unittest.main()
